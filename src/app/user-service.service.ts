import { Injectable } from '@angular/core';
import { environment } from "../environments/environment";
import { HttpClient, HttpEvent } from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http: HttpClient) { }
  createuser(info) {
    let data = this.http.post(`${environment.api_url}/userRoute/createuser`, info)
    return data
  }
  updateuser(id, info) {
    let data = this.http.put(`${environment.api_url}/userRoute/updateuser/` + id, info)
    return data
  }
  findalluser() {
    let data = this.http.get(`${environment.api_url}/userRoute/findalluser`)
    return data
  }
  finduser(id) {
    let data = this.http.get(`${environment.api_url}/userRoute/finduser/` + id)
    return data
  }
}
