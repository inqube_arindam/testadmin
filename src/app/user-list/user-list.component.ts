import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service'
import { Router } from "@angular/router";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  list: any
  users: any
  constructor(private userServiceService: UserServiceService, private router: Router) { }

  ngOnInit() {
    this.userServiceService.findalluser().subscribe(res => {
      this.list = res
      this.users = this.list.data
    })
  }
  add() {
    this.router.navigate(['user/add'])
  }
  update(id) {
    this.router.navigate(['user/add', id])
  }

}
