import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { UserServiceService } from '../user-service.service'
import { ActivatedRoute } from '@angular/router';
import { Router } from "@angular/router";

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  rForm: FormGroup;
  picPath: any;
  id: any
  picPath2: any
  constructor(private userServiceService: UserServiceService, private fb: FormBuilder, private routeActivate: ActivatedRoute, private router: Router) {
    this.rForm = fb.group({
      'name': ['', Validators.required],
      'phone': ['', Validators.required],
      'e_mail': [''],
    });
  }

  ngOnInit() {
    this.id = this.routeActivate.snapshot.paramMap.get('id');
    if (this.id) {
      this.userServiceService.finduser(this.id).subscribe(res => {
        this.rForm = this.fb.group({
          'name': res['data'][0].name,
          'phone': res['data'][0].phone,
          'e_mail': res['data'][0].e_mail,
        });
        this.picPath2 = res['data'][0].file_path
      })
    }
  }
  add() {
    const formData = new FormData();
    formData.append('name', this.rForm.value.name)
    formData.append('e_mail', this.rForm.value.e_mail)
    formData.append('phone', this.rForm.value.phone)
    formData.append('photo', this.picPath)
    if (this.id) {
      this.userServiceService.updateuser(this.id, formData).subscribe((res) => {
        this.router.navigate([''])
      })
    }
    else {
      this.userServiceService.createuser(formData).subscribe((res) => {
        this.router.navigate([''])
      })
    }

  }
  uploadPic(event) {
    if (event.target.files) { this.picPath = event.target.files[0] }
    console.log("this.picPath", this.picPath)
  }

}
